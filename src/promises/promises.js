﻿describe('pizza pi', function() {
    var person = function(name, $log) {
        this.eat = function(food) {
            $log.info(name + "is eating food" + food);
        };
        this.beHungry = function(reason) {
            $log.warn(name + " is hungry becuase: " + reason);
        };
    };

    var $q, $exceptionHandler, $log, $rootScope;
    var servePreparedOrder, promiseOrder, pawel, pete;

    beforeEach(inject(function(_$q_, _$exceptionHandler_, _$log_, _$rootScope_) {
        $q = _$q_;
        $exceptionHandler = _$exceptionHandler_;
        $log = _$log_;
        $rootScope = _$rootScope_;
        pawel = new person('Pawel', $log);
        pete = new person('Peter', $log);
    }));

    describe('pizza pi', function() {
        describe('$q used directly', function() {
            it('should illustrate basic usage of q', function() {
                var pizzaOrderFulfillment = $q.defer();
                var pizzaDelivered = pizzaOrderFulfillment.promise;
                pizzaDelivered.then(pawel.eat, pawel.beHungry);

                pizzaOrderFulfillment.resolve('Margeritha');
                $rootScope.$digest();
                expect($log.info.logs).toContain(['Pawel is eating delicious Margherita']);
            });

        });
    });

    describe('q used in a service', function() {
        var Restaurant = function($q, $rootScope) {
            var currentOrder;
            this.takeOrder = function(ordereditems) {
                currentOrder = {
                    deffered: $q.defer(),
                    items: ordereditems
                };
                return currentOrder.deffered.promise;
            };

            this.deliverOrder = function() {
                currentOrder.deffered.resolve(currentOrder.items);
                $rootScope.$digest();
            };

            this.problemWithOrder = function(reason) {
                currentOrder.deffered.reject(reason);
                $rootScope.$digest();
            };

            var slice = function(pizza) {
                return "sliced" + pizza;
            };

            var pizzaPit, saladBar;
            beforeEach(function() {
                pizzaPit = new Restaurant($q, $rootScope);
                saladBar = new Restaurant($q, $rootScope);
            });

            it('should illustrate primise rejection', function() {

                pizzaPit = new Restaurant($q, $rootScope);
                var pizzaDelivered = pizzaPit.takeOrder('Capricciosa');
                pizzaDelivered.then(pawel.eat, pawel.beHungry);

                pizzaPit.problemWithOrder('no Capricciosa, only Margherita left');
                expect($log.warn.logs).toContain(['Pawel is hungry because: no Capricciosa, only Margherita left']);
            });



        };
    });
});